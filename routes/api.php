<?php

use App\Http\Controllers\API\V2\MediaProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth_api')->group(function () {
    Route::post('/products/images', [MediaProductController::class, 'store'])->name('products.images.store');
    Route::post('/products/images-from-link', [MediaProductController::class, 'storeImageFromLink'])->name('products.images.storeImageFromLink');
    Route::delete('/products/{productId}/images', [MediaProductController::class, 'deleteAllMediaForProduct'])->name('products.images.deleteAll');
    Route::delete('/products/images/{productId}/{file_name}', [MediaProductController::class, 'deleteSingleProductImage'])->name('media.delete');
    Route::get('/products/images/{productId}', [MediaProductController::class, 'getProductImages'])->name('products.images.get');
});
