<?php

namespace App\Http\Controllers\API\V2;

use App\Http\Controllers\Controller;
use App\Models\MediaProduct;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Image\Image;
use Intervention\Image\Facades\Image as InterventionImage;
use Illuminate\Support\Facades\File;


class MediaProductController extends Controller
{
    /**
     * Store the uploaded images for a product.
     *
     * @param Request $request The request object containing the uploaded images and other data.
     * @return JsonResponse The JSON response containing the stored images.
     */

    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'productId' => 'required|integer',
            'images' => 'required|array',
            'images.*' => 'required|image|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $productId = $request->input('productId');
        $priority = $request->has('priority') ? $request->input('priority') : 1;

        $images = [];

        if ($request->hasFile('images')) {
            $uploadedImages = $request->file('images');

            foreach ($uploadedImages as $uploadedImage) {
                $extension = $uploadedImage->getClientOriginalExtension() ?: $uploadedImage->guessExtension();
                $originalFilename = $uploadedImage->getClientOriginalName();

                $directory = "products/{$productId}";
                $path = $uploadedImage->storeAs($directory, $originalFilename, 'media');

                if (!$path) {
                    return response()->json(['error' => 'Failed to store image.'], 422);
                }

                // Шлях до оригінального зображення
                $originalImagePath = Storage::disk('media')->path($directory . '/' . $originalFilename);

                // Оптимізація та збереження оптимізованого зображення
                $optimizedFilename = "product_{$priority}.{$extension}";
                $optimizedImagePath = Storage::disk('media')->path($directory . '/' . $optimizedFilename);

                Image::load($originalImagePath)
                    ->optimize()
                    ->save($optimizedImagePath);

                // Видалення оригінального зображення
                Storage::disk('media')->delete($directory . '/' . $originalFilename);

                // Створення папки "conversions", якщо вона не існує
                $conversionsDirectory = "./products/{$productId}/conversions";
                Storage::disk('media')->makeDirectory($conversionsDirectory);

                // Шлях до конвертованого зображення
                $webpFilename = "product_{$priority}.webp";
                $webpImagePath = Storage::disk('media')->path($conversionsDirectory . '/' . $webpFilename);

                // Конвертація в формат WebP
                InterventionImage::make($optimizedImagePath)
                    ->save($webpImagePath, 80, 'webp');

                $mediaProduct = MediaProduct::create([
                    'product_id' => $productId,
                    'file_name' => $optimizedFilename,
                    'priority' => $priority,
                ]);

                $images[] = $mediaProduct;
                $priority++;
            }
        }

        return response()->json(['images' => $images], 200);
    }

    public function storeImageFromLink(Request $request): JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'productId' => 'required|integer',
            'images' => 'required|array',
            'images.*' => 'required|url'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $productId = $request->input('productId');
        $priority = $request->has('priority') ? $request->input('priority') : 1;

        $images = [];
        $imagesLinks = $request->input('images');

        foreach ($imagesLinks as $imageUrl) {
            $productId = $request->input('productId');

            // Отримання зображення
            $response = Http::get($imageUrl);
            $imageContent = $response->body();

            // Створення об'єкта Intervention Image
            $image = InterventionImage::make($imageContent);
            $extension = pathinfo($imageUrl, PATHINFO_EXTENSION);

            // Генеруємо унікальне ім'я
            $originalFilename = basename($imageUrl);

            // Створюємо директорію
            $directory = "./products/{$productId}";
            Storage::disk('media')->makeDirectory($directory);

            // Зберігаємо оригінальне зображення
            $originalImagePath = "{$directory}/{$originalFilename}";
            Storage::disk('media')->put($originalImagePath, $imageContent);

            if (!Storage::disk('media')->exists($originalImagePath)) {
                return response()->json(['error' => 'Failed to store image.'], 422);
            }

            // Оптимізація та збереження оптимізованого зображення
            $optimizedFilename = "product_{$priority}.{$extension}";
            $optimizedImagePath = Storage::disk('media')->path($directory . '/' . $optimizedFilename);

            $image->save($optimizedImagePath, 80);

            // Видаляємо оригінал
            Storage::disk('media')->delete($originalImagePath);

            // Створення папки "conversions", якщо вона не існує
            $conversionsDirectory = "./products/{$productId}/conversions";
            Storage::disk('media')->makeDirectory($conversionsDirectory);

            // Шлях до конвертованого зображення
            $webpFilename = "product_{$priority}.webp";
            $webpImagePath = Storage::disk('media')->path($conversionsDirectory . '/' . $webpFilename);

            // Конвертація в формат WebP
            InterventionImage::make($optimizedImagePath)
                ->save($webpImagePath, 80, 'webp');

            // Зберігаємо інформацію в БД
            $mediaProduct = MediaProduct::create([
                'product_id' => $productId,
                'file_name' => $optimizedFilename,
                'priority' => $priority,
            ]);

            $images[] = $mediaProduct;
            $priority++;

        }

        return response()->json(['images' => $images], 200);

    }
    /**
     * Delete all media for a product.
     *
     * @param int $productId The ID of the product.
     * @return JsonResponse The JSON response indicating the result of the deletion.
     */
    public function deleteAllMediaForProduct(int $productId): JsonResponse
    {
        $directory = "./products/{$productId}";

        // Видалення папки з усіма зображеннями
        Storage::disk('media')->deleteDirectory($directory);

        // Видалення записів про зображення з бази даних
        MediaProduct::where('product_id', $productId)->delete();

        return response()->json(['message' => 'All media for the product has been deleted.'], 200);
    }

    /**
     * Delete a single media item.
     *
     * @param int $productId The ID of the product.
     * @param string $file_name The filename of the optimized image.
     * @return JsonResponse The JSON response indicating the result of the deletion.
     */


    public function deleteSingleProductImage(int $productId, string $file_name): JsonResponse
    {
        $mediaItem = MediaProduct::where('product_id', $productId)
            ->where('file_name', $file_name)
            ->first();

        if (!$mediaItem) {
            return response()->json(['error' => 'Media item not found.'], 404);
        }

        $directory = "./{$productId}";
        $extension = File::extension($file_name);
        $fileNameWithoutExtension = File::name($file_name);

        // Видалення оригінального зображення
        Storage::disk('media')->delete($directory . '/' . $file_name);

        // Видалення конвертованого зображення
        $optimizedFileNameWithoutExtension = File::name($file_name);
        $optimizedFilePath = $directory . '/conversions/' . $optimizedFileNameWithoutExtension . '.webp';
        Storage::disk('media')->delete($optimizedFilePath);

        // Видалення прев'юшок
        $previewDirectory = "./products/{$productId}/preview";
        $previewFiles = Storage::disk('media')->files($previewDirectory);

        foreach ($previewFiles as $previewFile) {
            $previewFileName = File::basename($previewFile);
            if (Str::startsWith($previewFileName, $fileNameWithoutExtension)) {
                Storage::disk('media')->delete($previewFile);
            }
        }

        // Видалення запису про зображення з бази даних
        $mediaItem->delete();

        return response()->json(['message' => 'The media item has been deleted.'], 200);
    }

    /**
     * Get the URLs for all images associated with a specific product.
     *
     * @param int $productId The ID of the product.
     * @return JsonResponse The JSON response containing the image URLs.
     */
    public function getProductImages(int $productId): JsonResponse
    {
        $mediaProducts = MediaProduct::where('product_id', $productId)->get();
        $baseUrl = config('app.url');

        $images = [];

        foreach ($mediaProducts as $mediaProduct) {
            $optimizedFilename = $mediaProduct->file_name;
            $priority = $mediaProduct->priority;

            $originalImageUrl = $baseUrl . "/media/products/{$productId}/{$optimizedFilename}";
            $conversionsDirectory = "/media/products/{$productId}/conversions";

            $webpImageUrl = $baseUrl . "{$conversionsDirectory}/product_{$priority}.webp";

            $previewDirectory = "/media/products/{$productId}/preview";
            $previewImages = [];

            $previewSizes = [
                ['width' => 200, 'height' => 110],
                ['width' => 280, 'height' => 150],
                ['width' => 45, 'height' => 25],
            ];

            foreach ($previewSizes as $size) {
                $previewFilename = "product_{$priority}_{$size['width']}x{$size['height']}.webp";
                $previewKey = "preview_{$size['width']}x{$size['height']}";
                $previewImageUrl = $baseUrl . "{$previewDirectory}/{$previewFilename}";

                $previewImages[$previewKey] = $previewImageUrl;
            }

            $imageData = [
                'original' => $originalImageUrl,
                'webp' => $webpImageUrl,
                'preview' => $previewImages,
            ];

            $images[$priority] = $imageData;
        }

        return response()->json($images, 200);
    }
}