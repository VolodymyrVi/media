<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
/**
 * Class AuthenticateApi
 *
 * This class is a Laravel middleware for token-based authentication of microservices.
 */
class AuthenticateApi extends Middleware
{
    /**
     * Authenticate the request.
     *
     * @param Request $request
     * @param array $guards
     * @return void
     * @throws AuthenticationException
     */

    protected function authenticate($request, array $guards): void
    {
        $token = $request->query('api_token');

        if (empty($token)){
            $token = $request->input('api_token');
        }
        if (empty($token)){
            $token = $request->bearerToken();
        }

        if ($token === config('apitokens')[0]) return;
        $this->unauthenticated($request, $guards);
    }
}