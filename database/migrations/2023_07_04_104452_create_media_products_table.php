<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media_products', function (Blueprint $table) {
            $table->id();
            $table->string('file_name');
            $table->string('size')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedSmallInteger('priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media_products');
    }
};
